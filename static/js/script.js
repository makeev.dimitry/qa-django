function send(url, data, onload) {
  let xhr = new XMLHttpRequest();
  xhr.open("POST", url);
  xhr.setRequestHeader("Accept", "application/json");
  xhr.setRequestHeader("Content-Type", "application/json");
  xhr.setRequestHeader("X-CSRFToken", document.querySelector('[name=csrfmiddlewaretoken]').value);
  xhr.onload = () => {
    resp = xhr.responseText;
    try {
      onload(JSON.parse(resp))
    } catch {
      console.log(resp);
    }
  }
  xhr.send(JSON.stringify(data));
}

function vote_question(questionId, vote) {
  send(`/qa/${questionId}/vote/`,
      {"id": questionId,"vote": vote},
      function(data) {
        document.getElementById('question_score').innerHTML = data.score;
      });
}

function vote_answer(answerId, vote) {
  send(`/qa/answer/${answerId}/vote/`,
      {"id": answerId,"vote": vote},
      function(data) {
        document.getElementById(`answer_${answerId}_score`).innerHTML = data.score;
      });
}