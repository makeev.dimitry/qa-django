from django.contrib.auth.forms import AuthenticationForm
from django.shortcuts import render, redirect
from django.contrib.auth import login, authenticate, logout
from django.contrib import messages

from .forms import RegisterForm, ProfileForm, UserForm


def profile_view(request):
    if request.method == "POST":
        user_form = UserForm(request.POST, instance=request.user)
        profile_form = ProfileForm(request.POST, request.FILES,
                                   instance=request.user.profile)
        if profile_form.is_valid():
            profile_form.save()
            messages.success(request, 'Аватар успешно изменён')
        else:
            messages.error(request, 'Не удалось изменить аватар')
        if user_form.is_valid():
            user_form.save()
            messages.success(request, 'Профиль успешно обновлён')
        else:
            messages.error(request, 'Не удалось изменить данные')
        return redirect("user:profile")
    else:
        if not request.user.is_authenticated:
            return redirect("user:login")
        user_form = UserForm(instance=request.user)
    profile_form = ProfileForm(instance=request.user.profile)
    return render(request=request, template_name="user/profile.html",
                  context={"user": request.user,
                           "user_form": user_form,
                           "profile_form": profile_form})


def login_request(request):
    if request.method == "POST":
        form = AuthenticationForm(request, data=request.POST)
        if form.is_valid():
            username = form.cleaned_data.get('username')
            password = form.cleaned_data.get('password')
            user = authenticate(username=username, password=password)
            if user is not None:
                login(request, user)
                messages.success(request, f"Вы вошли на сайт {username}.")
                return redirect("index")
    else:
        form = AuthenticationForm()
    return render(request=request, template_name="user/login.html",
                  context={"login_form": form})


def register_request(request):
    if request.method == "POST":
        form = RegisterForm(request.POST, request.FILES)
        if form.is_valid():
            user = form.save()
            login(request, user)
            messages.success(request, "Успешная регистрация")
            profile_form = ProfileForm(request.POST, request.FILES,
                                       instance=request.user.profile)
            if profile_form.is_valid():
                profile_form.save()
            return redirect("index")
        messages.error(request, f"Не удалось зарегистрироваться")
    else:
        form = RegisterForm()
    return render(request=request, template_name="user/register.html",
                  context={"register_form": form})


def logout_request(request):
    logout(request)
    messages.warning(request, "Произошёл выход из учётной записи")
    return redirect("index")
