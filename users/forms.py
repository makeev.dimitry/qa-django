from django import forms
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User
from django.core.files.images import get_image_dimensions

from .models import Profile


class UserForm(forms.ModelForm):
    class Meta:
        model = User
        fields = ['email']


class ProfileForm(forms.ModelForm):
    class Meta:
        model = Profile
        fields = ['avatar']

    def clean_avatar(self):
        avatar = self.cleaned_data['avatar']
        if avatar:
            avatar.name = f'profile.{avatar.name.split(".")[-1]}'
            try:
                w, h = get_image_dimensions(avatar)
                max_width = max_height = 2048
                if w > max_width or h > max_height:
                    raise forms.ValidationError(
                        f'Допустимый размер изображения до'
                        f' {max_width}x{max_height} точек')
                if len(avatar) > (500 * 1024):
                    raise forms.ValidationError('Размер файла больше 100 кб')

            except AttributeError as error:
                print(error)
        return avatar or None

    def save(self, commit=True):
        profile = super(ProfileForm, self).save(commit=False)
        profile.avatar = self.clean_avatar()
        if commit:
            profile.save()
        return profile


class RegisterForm(UserCreationForm):
    email = forms.EmailField(required=True)
    avatar = forms.ImageField(required=False)

    class Meta:
        model = User
        fields = ("username", "email", "password1", "password2", "avatar")

    def save(self, commit=True):
        user = super(RegisterForm, self).save(commit=False)
        user.email = self.cleaned_data['email']
        if commit:
            user.save()
        return user
