from django.urls import path

from . import views

app_name = 'qa'
urlpatterns = [
    path('ask', views.ask_view, name='ask'),
    path('<int:question_id>/', views.QuestionView.as_view(), name='question'),
    path('<int:question_id>/vote/', views.vote, name='vote'),
    path('answer/<int:answer_id>/vote/', views.vote_answer, name='vote_answer'),
    path('answer/<int:answer_id>/', views.correct_answer, name='correct_answer'),
    path('', views.SearchView.as_view(), name='search')
]
