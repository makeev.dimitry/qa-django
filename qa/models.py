from django.contrib.auth.models import User
from django.contrib.contenttypes.fields import GenericRelation
from django.db import models
from django.utils import timezone


class Tag(models.Model):
    name = models.CharField('Имя тега', max_length=255)

    def __str__(self):
        return self.name


class AutoDateTimeField(models.DateTimeField):
    def pre_save(self, model_instance, add):
        return timezone.now()


class Question(models.Model):
    title = models.CharField('Заголовок вопроса', max_length=255)
    text = models.TextField('Текст вопроса')

    user = models.ForeignKey(User, on_delete=models.CASCADE)
    created = models.DateTimeField('Дата создания', default=timezone.now)
    updated = AutoDateTimeField('Дата обновления')

    votes = GenericRelation('VoteQuestion')
    score = models.IntegerField(default=0)
    is_answered = models.BooleanField('Ответ получен', default=False)

    tags = models.ManyToManyField(Tag)

    def __str__(self):
        return self.title

    def vote(self, user, value):
        obj = VoteQuestion.objects.filter(question=self, user=user).first()
        if obj:
            self.score -= obj.value
            obj.value = value
            obj.save()
        else:
            VoteQuestion(user=user, value=value, question=self).save()
        self.score += value
        self.save()
        return self.score


class Answer(models.Model):
    text = models.TextField('Текст ответа')
    question = models.ForeignKey(Question, on_delete=models.CASCADE)
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    created = models.DateTimeField('Дата ответа', default=timezone.now)

    votes = GenericRelation('VoteAnswer')
    score = models.IntegerField(default=0)
    is_answered = models.BooleanField('Является ответом', default=False)

    def __str__(self):
        return self.text

    def vote(self, user, value):
        obj = VoteAnswer.objects.filter(answer=self, user=user).first()
        if obj:
            self.score -= obj.value
            obj.value = value
            obj.save()
        else:
            VoteAnswer(user=user, value=value, answer=self).save()
        self.score += value
        self.save()
        return self.score


class VoteQuestion(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    value = models.IntegerField(default=0)
    question = models.ForeignKey(Question, on_delete=models.CASCADE)


class VoteAnswer(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    value = models.IntegerField(default=0)
    answer = models.ForeignKey(Answer, on_delete=models.CASCADE)

