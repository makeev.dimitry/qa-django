import json
from json import JSONDecodeError

from django.contrib import messages
from django.core.mail import send_mail
from django.core.paginator import EmptyPage, PageNotAnInteger, Paginator
from django.db.models import Q
from django.http import HttpResponseRedirect, JsonResponse
from django.shortcuts import render, get_object_or_404, get_list_or_404, \
    redirect
from django.urls import reverse
from django.views.decorators.http import require_POST
from django.views.generic.base import TemplateView

from qa.models import Question, Answer, Tag
from qa.forms import QuestionForm, AnswerForm


def ask_view(request):
    if request.method == "POST":
        form = QuestionForm(request.POST)
        if form.is_valid():
            question = form.save(commit=False)
            question.user = request.user
            tags = form.cleaned_data.get('tags', []).split(',')
            tag_objs = []
            if len(tags) <= 3:
                for tag in tags:
                    obj, _ = Tag.objects.get_or_create(name=tag.strip())
                    tag_objs.append(obj)
                question.save()
                question.tags.set(tag_objs)
                question.save()
                messages.success(request, 'Вопрос успешно создан')
                return HttpResponseRedirect(reverse('qa:question',
                                                    args=(question.id,)))
            else:
                messages.error(request, 'Допускается не более 3 тегов')
                form.add_error('tags', 'Допускается не более 3 тегов')
        else:
            messages.error(request, 'Не удалось создать вопрос')
    else:
        form = QuestionForm()

    form.fields['tags'].label = 'Список тегов (через запятую)'
    return render(request=request,
                  template_name="qa/ask.html",
                  context={"form": form})


def process_paginator(self, paginator):
    page = self.request.GET.get("page")
    try:
        show_lines = paginator.page(page)
    except PageNotAnInteger:
        show_lines = paginator.page(1)
    except EmptyPage:
        show_lines = paginator.page(paginator.num_pages)
    return show_lines


class QuestionView(TemplateView):
    model = Question
    template_name = "qa/question.html"

    def get_context_data(self, question_id, **kwargs):
        context = super().get_context_data(**kwargs)
        question = get_object_or_404(Question, pk=question_id)
        answers = question.answer_set.order_by('-is_answered',
                                               '-score', '-created')
        context['question'] = question
        context['form'] = AnswerForm()
        context['answers'] = answers

        paginator = Paginator(answers, 30)
        context["lines"] = process_paginator(self, paginator)
        return context

    def post(self, request, *args, question_id, **kwargs):
        form = AnswerForm(request.POST)
        if form.is_valid():
            answer = form.save(commit=False)
            answer.user = request.user
            answer.question = get_object_or_404(Question, pk=question_id)
            answer.save()
            self.notify_user(answer)
            return HttpResponseRedirect(reverse('qa:question',
                                                args=(question_id,)))
        else:
            context = self.get_context_data()
            return self.render_to_response(context)

    @staticmethod
    def notify_user(answer):
        subject = "Ответ на вопрос"
        message = "Кто-то ответил на вопрос"
        send_mail(subject, message, None, [answer.question.user.email])


class IndexView(TemplateView):
    model = Question
    template_name = 'qa/index.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        questions = Question.objects.order_by('-created', '-score')
        context['questions'] = questions

        paginator = Paginator(questions, 20)
        context["lines"] = process_paginator(self, paginator)
        return context


class SearchView(TemplateView):
    model = Question
    template_name = 'qa/search.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        questions = []
        search = self.request.GET.get('search')
        if search:
            if search.startswith('tag:'):
                tag = search.split(':')[1]
                questions = Question.objects.\
                    order_by('-score', '-created').\
                    filter(Q(tags__name=tag))
            else:
                questions = Question.objects.\
                    order_by('-score', '-created'). \
                    filter(
                        Q(text__icontains=search) |
                        Q(title__icontains=search)
                    )
        context['questions'] = questions

        paginator = Paginator(questions, 20)
        context["lines"] = process_paginator(self, paginator)
        return context


def parse_request(request):
    data = {}
    try:
        data = json.loads(request.body)
    except JSONDecodeError:
        pass
    return data


@require_POST
def vote(request, question_id):
    data = parse_request(request)
    question = Question.objects.get(pk=question_id)
    score = question.vote(request.user, int(data['vote']))
    return JsonResponse({'score': score})


@require_POST
def vote_answer(request, answer_id):
    data = parse_request(request)
    answer = Answer.objects.get(pk=answer_id)
    score = answer.vote(request.user, int(data['vote']))
    return JsonResponse({'score': score})


def trending(request):
    return {'trending': Question.objects.order_by('-score')[:10]}


def correct_answer(request, answer_id):
    answer = get_object_or_404(Answer, pk=answer_id)
    question = answer.question
    if not question.is_answered and question.user == request.user:
        answer.is_answered = True
        answer.save()
        question.is_answered = True
        question.save()
    else:
        messages.error(request, 'Не удалось отметить ответ как верный')
    return redirect('qa:question', answer.question.id)

