from django.contrib import admin

from .models import Tag, Question, Answer


class QuestionAdmin(admin.ModelAdmin):
    list_display = ('title', 'text', 'user', 'created', 'updated')
    list_filter = ['created']
    search_fields = ['question_text']


class AnswerAdmin(admin.ModelAdmin):
    list_display = ('question', 'text', 'user', 'created', 'is_answered')
    list_filter = ['created']
    search_fields = ['question_text']


admin.site.register(Question, QuestionAdmin)
admin.site.register(Tag)
admin.site.register(Answer, AnswerAdmin)
