from rest_framework.fields import ReadOnlyField, BooleanField
from rest_framework.relations import SlugRelatedField
from rest_framework.serializers import ModelSerializer

from qa.models import Question, Answer


class QuestionSerializer(ModelSerializer):
    user = ReadOnlyField(source="user.username")
    avatar = ReadOnlyField(source="user.profile.avatar.url")
    answered = BooleanField(source="is_answered")
    tags = SlugRelatedField(slug_field="name", many=True, read_only=True)

    class Meta:
        model = Question
        fields = ["title", "text", "user", "avatar", "created",
                  "score", "answered", "tags"]


class AnswerSerializer(ModelSerializer):
    user = ReadOnlyField(source="user.username")
    avatar = ReadOnlyField(source="user.profile.avatar.url")
    correct_answer = BooleanField(source="is_answered")

    class Meta:
        model = Answer
        fields = ["text", "user", "avatar",
                  "created", "score", "correct_answer"]





