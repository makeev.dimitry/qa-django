from django.urls import path
from django.views.generic import TemplateView
from rest_framework.authtoken.views import obtain_auth_token
from rest_framework.schemas import get_schema_view

from . import views


urlpatterns = [
    path('index/', views.IndexView.as_view(), name='index'),
    path('search/', views.SearchView.as_view(), name='search'),
    path('trending/', views.TrendingView.as_view(), name='trending'),
    path('question/<int:pk>', views.QuestionView.as_view(), name='question'),
    path('question/<int:pk>/answers', views.AnswersView.as_view(), name='answers'),

    # Аутентификация (токен)
    # http post http://localhost:8000/api/auth/ username=[user] password=[pass]
    path('auth/', obtain_auth_token, name='auth'),

    path('docs/', TemplateView.as_view(
        template_name='swagger.html',
        extra_context={'schema_url': 'openapi-schema'}
    ), name='swagger'),
    path('spec/', get_schema_view(
        title="QA",
        description="API вопросов и ответов",
        version="1.0",
        url="/"
    ), name='openapi-schema'),
]
