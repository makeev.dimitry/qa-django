import coreapi
import coreschema
from django.core.paginator import Paginator
from django.db.models import Q
from rest_framework.generics import get_object_or_404
from rest_framework.response import Response
from rest_framework import status
from rest_framework.schemas import AutoSchema, openapi, ManualSchema
from rest_framework.views import APIView

from qa.models import Question
from .serializers import QuestionSerializer, AnswerSerializer


class PaginationSchema(AutoSchema):
    def get_manual_fields(self, path, method):
        return [
                coreapi.Field(
                    "page",
                    required=True,
                    location="query",
                    schema=coreschema.Integer(),
                    type='Integer',
                    description='Текущая страница',
                    example='1',
                ),
                coreapi.Field(
                    "results",
                    required=True,
                    location="query",
                    schema=coreschema.Integer(),
                    type='Integer',
                    description='Количество результатов на странице',
                    example='1',
                )
            ]


class IndexView(APIView):
    """
    Получение вопросов на главной странице (с пагинацией).
    """
    schema = PaginationSchema()

    def get(self, request):
        questions = Question.objects.order_by('-created', '-score')
        page_number = self.request.query_params.get('page', 1)
        page_size = self.request.query_params.get('results', 5)
        paginator = Paginator(questions, page_size)
        serializer = QuestionSerializer(paginator.page(page_number), many=True)
        return Response(serializer.data, status=status.HTTP_200_OK)


class TrendingView(APIView):
    def get(self, request):
        questions = Question.objects.order_by('-score')[:10]
        serializer = QuestionSerializer(questions, many=True)
        return Response(serializer.data, status=status.HTTP_200_OK)


class QuestionView(APIView):
    """ Получить вопрос """
    @staticmethod
    def get(request, pk):
        question = get_object_or_404(Question, pk=pk)
        return Response(QuestionSerializer(question).data)


class AnswersView(APIView):
    """ Получить ответы на вопрос """
    schema = PaginationSchema()

    def get(self, request, pk):
        question = get_object_or_404(Question, pk=pk)
        answers = question.answer_set.order_by('-is_answered',
                                               '-score', '-created')
        page_number = self.request.query_params.get('page', 1)
        page_size = self.request.query_params.get('results', 5)
        paginator = Paginator(answers, page_size)
        serializer = AnswerSerializer(paginator.page(page_number), many=True)
        return Response(serializer.data, status=status.HTTP_200_OK)


class SearchView(APIView):
    """ Поисковый запрос """
    schema = PaginationSchema()

    def get(self, request):
        search = self.request.GET.get('search')
        questions = []
        if search:
            if search.startswith('tag:'):
                tag = search.split(':')[1]
                questions = Question.objects.\
                    order_by('-score', '-created').\
                    filter(Q(tags__name=tag))
            else:
                questions = Question.objects.\
                    order_by('-score', '-created'). \
                    filter(
                        Q(text__icontains=search) |
                        Q(title__icontains=search)
                    )
        page_number = self.request.query_params.get('page', 1)
        page_size = self.request.query_params.get('results', 5)
        paginator = Paginator(questions, page_size)
        serializer = QuestionSerializer(paginator.page(page_number), many=True)
        return Response(serializer.data, status=status.HTTP_200_OK)
